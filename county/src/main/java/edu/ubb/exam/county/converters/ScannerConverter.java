package edu.ubb.exam.county.converters;

import edu.ubb.exam.county.models.ScannerDto;

public class ScannerConverter {
    public static String scannerDtoToString(ScannerDto scannerDto) {
        return scannerDto.getName() + "," + scannerDto.getA() + "," + scannerDto.getB() + "," + scannerDto.getC();
    }
}
