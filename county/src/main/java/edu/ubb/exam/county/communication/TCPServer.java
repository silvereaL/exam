package edu.ubb.exam.county.communication;

import edu.ubb.exam.county.models.County;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.function.UnaryOperator;

public class TCPServer {
    private final ExecutorService executorService;
    private final Map<String, UnaryOperator<Message>> methodHandlers;
    private final int port;
    private County county;

    public TCPServer(ExecutorService executorService, County county) {
        this.methodHandlers = new HashMap<>();
        this.executorService = executorService;
        this.county = county;
        this.port = county.getId();
    }

    public void addHandler(String methodName, UnaryOperator<Message> handler) {
        methodHandlers.put(methodName, handler);
    }

    public void refreshVotes(int a, int b, int c) {
        this.county.setA(this.county.getA() + a);
        this.county.setB(this.county.getB() + b);
        this.county.setC(this.county.getC() + c);
        this.county.refreshVoteCount();
    }

    public void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            this.executorService.submit(new ServerUpdate(this.county));
            while (true) {
                Socket client = serverSocket.accept();
                if (!executorService.submit(new ClientHandler(client)).get()) break;
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class ClientHandler implements Callable<Boolean> {
        private final Socket socket;

        public ClientHandler(Socket client) {
            this.socket = client;
        }

        @Override
        public Boolean call() {
            try (InputStream is = socket.getInputStream();
                 OutputStream os = socket.getOutputStream()) {
                Message request = new Message();
                request.readFrom(is);
                System.out.println("server received request from client: " + request);

                System.out.println(request.getHeader());
                System.out.println(request.getBody());

                Message response = methodHandlers.get(request.getHeader()).apply(request);

                response.writeTo(os);
                System.out.println("server sending back: " + response);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    private static class ServerUpdate implements Callable<Boolean> {
        private int lastVoteCount;
        private County county;

        public ServerUpdate(County county) {
            this.lastVoteCount = 0;
            this.county = county;
        }

        @Override
        public Boolean call() throws InterruptedException {
            while (true) {
                if (this.county.getVoteCount() > this.lastVoteCount) {
                    this.lastVoteCount = this.county.getVoteCount();
                    System.out.println("Server updated!");
                } else {
                    System.out.println("didn't update");
                }
                Thread.sleep(5000);
            }
        }
    }
}
