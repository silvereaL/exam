package edu.ubb.exam.county.communication;

import edu.ubb.exam.county.service.ScannerService;

public class MethodHandler {
    private final TCPServer server;
    private final ScannerService scannerService;
    boolean keepGoing = true;
    String toShutdown;

    public MethodHandler(TCPServer server, ScannerService scannerService) {
        this.server = server;
        this.scannerService = scannerService;
    }

    public void addHandlers() {
        addScannerHandler();
        serverShutdownHandler();
    }

    private void addScannerHandler() {
        server.addHandler(
                MessageHeader.BULLETIN_ADD,
                (request) -> {
                    System.out.println("received" + request.getBody());
                    String[] parsedRequest = request.getBody().split(",");
                    String name = parsedRequest[0];
                    int a = Integer.parseInt(parsedRequest[1]);
                    int b = Integer.parseInt(parsedRequest[2]);
                    int c = Integer.parseInt(parsedRequest[3]);
                    try {
                        scannerService.printScannerData(name, a, b, c);
                        server.refreshVotes(a, b, c);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!keepGoing) {
                        if (parsedRequest[0].equals(toShutdown)) {
                            System.out.println("dying");
                            return new Message(MessageHeader.CLIENT_SHUTDOWN, toShutdown);
                        }
                    }
                    return new Message(MessageHeader.OK_REQUEST, "");
                });
    }

    private void serverShutdownHandler() {
        server.addHandler(
                MessageHeader.CLIENT_SHUTDOWN,
                (request) -> {
                    System.out.println("SHUTDOWN!!!!");
                    System.out.println("received" + request.getBody());
                    keepGoing = false;
                    toShutdown = request.getBody();
                    System.out.println(toShutdown);
                    return new Message(MessageHeader.BAD_REQUEST, "");
                });

    }
}
