package edu.ubb.exam.county;

import edu.ubb.exam.county.communication.MethodHandler;
import edu.ubb.exam.county.communication.TCPServer;
import edu.ubb.exam.county.models.County;
import edu.ubb.exam.county.service.ScannerService;
import edu.ubb.exam.county.ui.Console;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerApp {
    public static void main(String[] args) {
        System.out.println("server started");
        County county = new County();
        Console console = new Console(county);
        console.readData();

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        ScannerService sensorService = new ScannerService();
        sensorService.setExecutorService(executorService);
        try {
            TCPServer tcpServer = new TCPServer(executorService, county);
            MethodHandler methodHandler = new MethodHandler(tcpServer, sensorService);
            methodHandler.addHandlers();
            tcpServer.startServer();
            executorService.shutdown();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }
}
