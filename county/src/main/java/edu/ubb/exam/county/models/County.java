package edu.ubb.exam.county.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class County {
    private String name;
    private int id;
    private int voteCount;
    private int a, b, c;

    public void refreshVoteCount() {
        this.voteCount = this.a + this.b + this.c;
    }
}
