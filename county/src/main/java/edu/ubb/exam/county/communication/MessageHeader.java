package edu.ubb.exam.county.communication;

public class MessageHeader {
    public static final String BULLETIN_ADD = "BULLETIN_ADD";
    public static final String CLIENT_SHUTDOWN = "CLIENT_SHUTDOWN";
    public static final String OK_REQUEST = "OK_REQUEST";
    public static final String BAD_REQUEST = "BAD_REQUEST";
}
