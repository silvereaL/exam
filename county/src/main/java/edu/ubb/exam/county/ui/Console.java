package edu.ubb.exam.county.ui;

import edu.ubb.exam.county.models.County;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private County county;

    public Console(County county) {
        this.county = county;
    }

    public void readData() {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter county: ");
            String county = input.readLine().strip();
            System.out.println("Enter id: ");
            int id = Integer.parseInt(input.readLine().strip());
            this.county.setId(id);
            this.county.setName(county);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
