package edu.ubb.exam.county.service;

import java.util.Random;
import java.util.concurrent.ExecutorService;

public class ScannerService {
    private ExecutorService executorService;

    public void printScannerData(String name, int a, int b, int c) throws InterruptedException {
        System.out.println(name);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        Thread.sleep(new Random().nextInt(4000));
        executorService.submit(() -> true);
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}
