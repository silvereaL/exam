package edu.ubb.exam.scanner.service;

import edu.ubb.exam.county.communication.Message;
import edu.ubb.exam.county.communication.MessageHeader;
import edu.ubb.exam.county.converters.ScannerConverter;
import edu.ubb.exam.county.models.Scanner;
import edu.ubb.exam.county.models.ScannerDto;
import edu.ubb.exam.scanner.communication.TCPClient;

import java.util.Random;
import java.util.concurrent.ExecutorService;

public class ScannerService {
    private final ExecutorService executorService;
    private final TCPClient tcpClient;

    public ScannerService(ExecutorService executorService, TCPClient tcpClient) {
        this.executorService = executorService;
        this.tcpClient = tcpClient;
    }

    private static int bulletinCount = 0;

    public void sendData(String name, int id) throws InterruptedException {
        this.tcpClient.setPort(id);
        while (true) {
            Random rd = new Random();
            int a = rd.nextInt(2);
            int b = rd.nextInt(2);
            int c = rd.nextInt(2);
            bulletinCount++;
            Thread.sleep(rd.nextInt(8000));
            System.out.println(bulletinCount + " - " + name + " " + a + " " + b + " " + c);
            ScannerDto dto = ScannerDto.builder().name(name).a(a).b(b).c(c).build();
            executorService.submit(
                    () -> {
                        Message request =
                                new Message(MessageHeader.BULLETIN_ADD, ScannerConverter.scannerDtoToString(dto));
                        Message response = tcpClient.sendAndReceive(request);
                        if (response.getHeader().equals(MessageHeader.CLIENT_SHUTDOWN)
                                && response.getBody().equals(name)) {
                            System.out.println(response.getHeader());
                            System.out.println(response.getBody());
                        }
                    });
        }
    }
}
