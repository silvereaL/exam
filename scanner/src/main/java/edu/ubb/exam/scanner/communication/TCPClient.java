package edu.ubb.exam.scanner.communication;

import edu.ubb.exam.county.communication.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TCPClient {
    private int port;

    public TCPClient() {
        this.port = Message.PORT;
    }

    public TCPClient(int port) {
        this.port = port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Message sendAndReceive(Message request) {
        try (Socket socket = new Socket(Message.HOST, this.port);
             InputStream is = socket.getInputStream();
             OutputStream os = socket.getOutputStream()) {
            System.out.println("sendAndReceive - sending request: " + request);
            request.writeTo(os);

            System.out.println("sendAndReceive - received response: ");
            Message response = new Message();
            response.readFrom(is);
            System.out.println(response);

            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
