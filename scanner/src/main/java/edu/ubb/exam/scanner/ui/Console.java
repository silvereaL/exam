package edu.ubb.exam.scanner.ui;

import edu.ubb.exam.scanner.service.ScannerService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    private final ScannerService service;

    public Console(ScannerService service) {
        this.service = service;
    }

    public void readData() {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter scanner name: ");
            String name = input.readLine().strip();
            System.out.println("Enter county id: ");
            int id = Integer.parseInt(input.readLine().strip());
            this.service.sendData(name, id);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
