package edu.ubb.exam.scanner;

import edu.ubb.exam.scanner.communication.TCPClient;
import edu.ubb.exam.scanner.service.ScannerService;
import edu.ubb.exam.scanner.ui.Console;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApp {
    public static void main(String[] args) {
        ExecutorService executorService =
                Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        TCPClient tcpClient = new TCPClient();

        ScannerService service = new ScannerService(executorService, tcpClient);
        Console console = new Console(service);

        console.readData();
    }
}
